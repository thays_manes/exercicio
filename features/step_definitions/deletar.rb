Dado(/^que esteja no cadastro de um veiculo$/) do
  page(Consulta).consulta
end

Quando(/^deletar um veiculo$/) do
  page(Deletar).deletar_veiculo
end

Então(/^apresenta mensagem de deletar com sucesso$/) do
  page(Comum).validar_texto_na_tela ('Carro excluído com sucesso.')
end
