Dado(/^que esteja no cadastro de veiculos$/) do
  page(Consulta).consulta
end

Quando(/^alterar o dados cadastrais do veiculo$/) do
  page(Alterar).alterar_dados
end

Então(/^apresenta uma mensagem de alterar com sucesso$/) do
  page(Comum).validar_texto_na_tela('Carro alterado com sucesso.')
end
