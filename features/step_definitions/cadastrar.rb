Dado(/^que esteja na tela de cadastro$/) do
   page(Cadastro).cadastro_de_veiculo
end

Quando(/^preencher com todas informações necessarias$/) do
  page(Cadastro).preencher_cadastro
end

Então(/^apresenta mensagem de cadastro com sucesso$/) do
  page(Comum).validar_texto_na_tela('Carro cadastrado com sucesso')
end
