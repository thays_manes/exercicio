#encoding: utf-8
#language: pt

Funcionalidade: Cadastrar um Veiculo
  Eu como cliente
  Quero cadastrar o veiculo
  Para que fique disponivel as informações sobre o veiculo

Cenario: Cadastro de Veiculo no app
  Dado que esteja na tela de cadastro
  Quando preencher com todas informações necessarias
  Então apresenta mensagem de cadastro com sucesso
