#encoding: utf-8
#language: pt

Funcionalidade: Alteração de dados
  Eu como cliente
  Quero poder alterar os dados do meu veiculo
  Para atualizar as informações

Cenario: Alterar dados do veiculo
  Dado que esteja no cadastro de veiculos
  Quando alterar o dados cadastrais do veiculo
  Então apresenta uma mensagem de alterar com sucesso
