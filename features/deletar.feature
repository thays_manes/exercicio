#encoding: utf-8
#language: pt

Funcionalidade: Excluir um Cadastro
  Eu como cliente
  Quero deletar os dados do veiculo
  Para que nao apareça mais no cadastro

Cenario: Deletar veiculo cadastrado
  Dado que esteja no cadastro de um veiculo
  Quando deletar um veiculo
  Então apresenta mensagem de deletar com sucesso
