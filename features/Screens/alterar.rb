class Alterar < Calabash::ABase

  def alterar_dados
    sleep 3
    wait_for_element_exists("* id:'km'", timeout:10)
    touch ("* id:'km'")
    clear_text
    keyboard_enter_text "72000"
    hide_soft_keyboard
    wait_for_element_exists("* id:'alterar'", timeout:10)
    touch ("* id:'alterar'")
  end
end
