class Consulta < Calabash::ABase

  def consulta
    touch ("* id:'consulta'")
    wait_poll(:until_exists => "* {text CONTAINS[c] 'Onix'}"){pan_up}
    touch ("* {text CONTAINS[c] 'Onix'}")
  end

end
