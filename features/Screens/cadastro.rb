class Cadastro < Calabash::ABase

  def cadastro_de_veiculo
    touch("* id:'cadastrar'")
  end

  def preencher_cadastro
    touch("* id:'modelo'")
    keyboard_enter_text "Onix"
    touch("* id:'ano'")
    keyboard_enter_text "2017"
    touch("* id:'placa'")
    keyboard_enter_text "THA1105"
    touch("* id:'km'")
    keyboard_enter_text "20000"
    hide_soft_keyboard
    touch("* id:'valor'")
    keyboard_enter_text "65000"
    hide_soft_keyboard
    touch("* id:'cadastrar'")
  end
end
